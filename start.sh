#!/bin/bash

set -eu -o pipefail

echo "==> Startup"

echo "==> Ensure directories"
mkdir -p /app/data/data/ /app/data/tpl /app/data/plugins /run/shaarli/cache/ /run/shaarli/pagecache/ /run/shaarli/tmp/ /run/shaarli/sessions

echo "==> Protect folders from public access"
# distribute .htaccess to r/w dirs
cp -u /app/code/data.orig/.htaccess /app/data/data/
cp -u /app/code/data.orig/.htaccess /run/shaarli/cache/
cp -u /app/code/data.orig/.htaccess /run/shaarli/pagecache/
cp -u /app/code/data.orig/.htaccess /run/shaarli/tmp/

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

echo "==> Update template directory"
for f in $(ls /app/code/tpl.orig); do
    rm -rf "/app/data/tpl/$f"
    ln -s "/app/code/tpl.orig/$f" "/app/data/tpl/$f"
done

echo "==> Update plugin directory"
for f in $(ls /app/code/plugins.orig); do
    rm -rf "/app/data/plugins/$f"
    ln -s "/app/code/plugins.orig/$f" "/app/data/plugins/$f"
done

if [[ -f "/app/data/data/config.json.php" ]]; then
    echo "==> Update configuration"
    sed -e 's/"check_updates": .*,/"check_updates": false,/'  \
        -e 's/"log": .*,/"log": "\/run\/shaarli\/shaarli.log",/'  \
        -i /app/data/data/config.json.php
fi

echo "==> Ensure directory permissions"
chown -R www-data:www-data /app/data /run/shaarli

echo "==> Starting apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
