### About

Shaarli is a personal, minimalist, super-fast, database free, bookmarking service.

Do you want to share the links you discover? Shaarli is a minimalist delicious clone
that you can install on your own server.

It is designed to be personal (single-user), fast and handy.

