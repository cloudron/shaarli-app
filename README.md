# Shaarli Cloudron App

This repository contains the Cloudron app package source for [Shaarli](https://github.com/shaarli/Shaarli).

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=com.github.shaarli)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id com.github.shaarli
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd shaarli-app

cloudron build
cloudron install
```

## Testing

The e2e tests are located in the `test/` folder and require [nodejs](http://nodejs.org/). They are creating a fresh build, install the app on your Cloudron, backup and restore. 

```
cd shaarli-app/test

npm install
USERNAME=<cloudron username> PASSWORD=<cloudron password> mocha --bail test.js
```

## LDAP

Shaarli is a single user app, so it's unclear if it makes sense to add LDAP auth. But it's there:

https://github.com/shaarli/Shaarli/pull/1428/commits

