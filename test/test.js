#!/usr/bin/env node

/* jslint node:true */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const username = 'admin';
    const password = 'changeme';
    const title = 'My Bookmarks';
    const testLink = 'https://cloudron.io';
    const testDescription = 'Run for it';

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function finishSetup() {
        await browser.get('https://' + app.fqdn);
        await waitForElement(By.xpath('//input[@name="setlogin"]'));
        await browser.findElement(By.xpath('//input[@name="setlogin"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@name="setpassword"]')).sendKeys(password);
        await browser.findElement(By.xpath('//input[@name="title"]')).sendKeys(title);
        await browser.findElement(By.xpath('//input[@name="Save"]')).click();
        await browser.sleep(3000);
        // const alert = browser.switchTo().alert();
        // await alert.accept();
    }

    async function login() {
        await browser.get('https://' + app.fqdn + '/?do=login');
        await waitForElement(By.xpath('//div[@id="login-form"]//input[@name="login"]'));
        await browser.findElement(By.xpath('//div[@id="login-form"]//input[@name="login"]')).sendKeys(username);
        await browser.findElement(By.xpath('//div[@id="login-form"]//input[@name="password"]')).sendKeys(password);
        await browser.findElement(By.xpath('//div[@id="login-form"]//input[@type="submit"]')).click();
        await waitForElement(By.xpath('//a[text()="Tools"]'));
    }

    async function logout() {
        await browser.get('https://' + app.fqdn + '/?do=logout');
        await waitForElement(By.xpath('//a[@id="login-button"]'));
    }

    async function addLink() {
        await browser.get('https://' + app.fqdn +'/?do=addlink');
        await waitForElement(By.xpath('//input[@name="post"]'));
        await browser.findElement(By.xpath('//input[@name="post"]')).sendKeys(testLink);
        const button = browser.findElement(By.xpath('//input[@type="submit"]'));
        await browser.executeScript('arguments[0].scrollIntoView(true)', button);
        await browser.findElement(By.xpath('//input[@type="submit"]')).click();
        await waitForElement(By.xpath('//textarea[@name="lf_description"]'));
        await browser.findElement(By.xpath('//textarea[@name="lf_description"]')).sendKeys(testDescription);
        const button2 = browser.findElement(By.xpath('//input[@name="save_edit"]'));
        await browser.executeScript('arguments[0].scrollIntoView(true)', button2);
        await browser.findElement(By.xpath('//input[@name="save_edit"]')).click();
        await waitForElement(By.xpath('//a[@href="' + testLink + '"]'));
    }

    async function linkExists() {
        await browser.get('https://' + app.fqdn +'/?do=daily');
        await waitForElement(By.xpath('//*[text()="' + testDescription + '"]'));
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.startsWith(LOCATION); })[0];
        expect(app).to.be.an('object');
    }

    xit('build app', function () {
        execSync('cloudron build', EXEC_ARGS);
    });

    it('install app', function () {
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can complete install', finishSetup);

    it('can login', login);
    it('can add link', addLink);
    it('link exists', linkExists);
    it('can logout', logout);

    it('can restart app', function () {
        execSync('cloudron restart --app ' + app.id);
    });

    it('can login', login);
    it('link exists', linkExists);
    it('can logout', logout);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, EXEC_ARGS);
    });

    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login);
    it('link exists', linkExists);
    it('can logout', logout);

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
        getAppInfo();
        expect(app).to.be.an('object');
    });

    it('can login', login);
    it('link exists', linkExists);
    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app', function () {
        execSync('cloudron install --appstore-id com.github.shaarli --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
    });
    it('can complete install', finishSetup);
    it('can login', login);
    it('can add link', addLink);
    it('link exists', linkExists);
    it('can logout', logout);

    it('can update', function () {
        execSync('cloudron update --app ' + LOCATION, EXEC_ARGS);
    });
    it('can login', login);
    it('link exists', linkExists);
    it('can logout', logout);
    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
