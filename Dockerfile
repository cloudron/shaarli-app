FROM cloudron/base:5.0.0@sha256:04fd70dbd8ad6149c19de39e35718e024417c3e01dc9c6637eaf4a41ec4e596c

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# renovate: datasource=github-releases depName=shaarli/Shaarli versioning=semver extractVersion=^v(?<version>.+)$
ARG SHAARLI_VERSION=0.14.0

RUN curl -L https://github.com/shaarli/Shaarli/releases/download/v${SHAARLI_VERSION}/shaarli-v${SHAARLI_VERSION}-full.tar.gz | tar -xz --strip-components 1 -f -

# these links will become valid after setup is run
RUN mv /app/code/data /app/code/data.orig && ln -sf /app/data/data /app/code/data && \
    mv /app/code/tpl /app/code/tpl.orig && ln -sf /app/data/tpl /app/code/tpl && \
    mv /app/code/plugins /app/code/plugins.orig && ln -sf /app/data/plugins/ /app/code/plugins && \
    rm -rf /app/code/cache && ln -sf /run/shaarli/cache /app/code/cache && \
    rm -rf /app/code/pagecache && ln -sf /run/shaarli/pagecache /app/code/pagecache && \
    rm -rf /app/code/tmp && ln -sf /run/shaarli/tmp /app/code/tmp

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf
RUN a2disconf other-vhosts-access-log
ADD apache/shaarli.conf /etc/apache2/sites-enabled/shaarli.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf
RUN a2enmod rewrite

# configure mod_php
RUN crudini --set /etc/php/8.3/apache2/php.ini PHP upload_max_filesize 8M && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP post_max_size 8M && \
    crudini --set /etc/php/8.3/apache2/php.ini PHP memory_limit 128M && \
    crudini --set /etc/php/8.3/apache2/php.ini Session session.save_path /run/shaarli/sessions && \
    crudini --set /etc/php/8.3/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.3/apache2/php.ini Session session.gc_divisor 100

RUN cp /etc/php/8.3/apache2/php.ini /etc/php/8.3/cli/php.ini

RUN ln -s /app/data/php.ini /etc/php/8.3/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.3/cli/conf.d/99-cloudron.ini

RUN chown -R www-data:www-data /app/code

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
